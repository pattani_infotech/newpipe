package org.schabi.newpipe;

import android.app.Application;

import com.streethawk.library.core.StreetHawk;
import com.streethawk.library.pointzi.Pointzi;

public class TestApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        // MultiDex.install(this);//PontZTest
        Pointzi.INSTANCE.init(this, "PattaniQA");
        StreetHawk.INSTANCE.tagString("sh_cuid", "gautam@pattaniinfotech.com");
    }
}
